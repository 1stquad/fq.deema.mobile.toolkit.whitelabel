﻿using System;
using AsyncDisplayKitBindings;
using Foundation;

namespace Softeq.XToolkit.WhiteLabel.iOS.Shared
{
    public class ObservableASTableDelegate : ASTableDelegate
    {
        public event EventHandler LastItemRequested;

        public override void WillDisplayNodeForRowAtIndexPath(ASTableView tableView, NSIndexPath indexPath)
        {
            var dataSource = tableView.TableNode.DataSource;

            if (dataSource == null)
            {
                throw new ArgumentNullException(nameof(dataSource));
            }

            if (CheckIsLastItem(indexPath.Row, GetItemsCount(dataSource, tableView.TableNode)))
            {
                LastItemRequested?.Invoke(this, EventArgs.Empty);
            }
        }

        protected virtual nint GetItemsCount(ASTableDataSource dataSource, ASTableNode tableNode)
        {
            return dataSource.NumberOfRowsInSection(tableNode, 0);
        }

        protected virtual bool CheckIsLastItem(int itemIndex, nint count)
        {
            return itemIndex == count - 1;
        }
    }
}
