﻿using System;
using Foundation;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using UIKit;

namespace Softeq.XToolkit.WhiteLabel.iOS.Shared
{
    /// <summary>
    /// Static storage for controlling the autorotation flag between modules.
    /// </summary>
    public static class Autorotate
    {
        private static bool _isEnabled;

        public static bool IsEnabled => _isEnabled;

        public static void Enable() => _isEnabled = true;

        public static void Disable() => _isEnabled = false;
    }

    public abstract class AutorotateAppDelegate : AppDelegateBase
    {
        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            Autorotate.Disable();

            return base.FinishedLaunching(application, launchOptions);
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations(UIApplication application, UIWindow forWindow)
        {
            if (forWindow == Window && Autorotate.IsEnabled)
            {
                return UIInterfaceOrientationMask.All;
            }
            return UIInterfaceOrientationMask.Portrait;
        }
    }

    public abstract class AutorotateViewControllerBase<TViewModel> : ViewControllerBase<TViewModel>
        where TViewModel : IViewModelBase
    {
        public AutorotateViewControllerBase(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Autorotate.Enable();
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
        {
            return UIInterfaceOrientationMask.All;
        }

        public override bool ShouldAutorotate()
        {
            return true;
        }
    }
}
