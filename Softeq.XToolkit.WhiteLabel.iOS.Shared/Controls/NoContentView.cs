﻿using System;
using Foundation;
using UIKit;

namespace Softeq.XToolkit.WhiteLabel.iOS.Shared.Controls
{
    [Register(nameof(NoContentView))]
    public class NoContentView : UIView
    {
        private UILabel _label;

        public NoContentView(IntPtr handle) : base(handle)
        {
#pragma warning disable RECS0021 // Warns about calls to virtual member functions occuring in the constructor
            Initialize();
#pragma warning restore RECS0021 // Warns about calls to virtual member functions occuring in the constructor
        }

        public string Text
        {
            get => _label.Text;
            set => _label.Text = value;
        }

        public UIColor TextColor
        {
            get => _label.TextColor;
            set => _label.TextColor = value;
        }

        protected virtual void Initialize()
        {
            TranslatesAutoresizingMaskIntoConstraints = false;

            _label = new UILabel
            {
                TextAlignment = UITextAlignment.Center,
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap,
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            AddSubview(_label);

            NSLayoutConstraint.ActivateConstraints(new[]
            {
                _label.LeadingAnchor.ConstraintEqualTo(LeadingAnchor, 16),
                _label.TrailingAnchor.ConstraintEqualTo(TrailingAnchor, -16),
                _label.CenterXAnchor.ConstraintEqualTo(CenterXAnchor),
                _label.CenterYAnchor.ConstraintEqualTo(CenterYAnchor)
            });
        }
    }
}
