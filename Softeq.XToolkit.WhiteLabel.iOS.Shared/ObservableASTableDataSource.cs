﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading;
using AsyncDisplayKitBindings;
using Foundation;
using UIKit;
using Softeq.XToolkit.Common.Collections;
using Softeq.XToolkit.Common.WeakSubscription;
using Softeq.XToolkit.Common;

namespace Softeq.XToolkit.WhiteLabel.iOS.Shared
{
    public class ObservableASTableDataSource<T> : ASTableDataSource
    {
        private readonly Func<int, IList<T>, ASCellNode> _createCellFunc;
        private readonly Thread _mainThread;
        private readonly WeakReferenceEx<ASTableNode> _tableNodeRef;

        private ObservableRangeCollection<T> _dataSource;
        private INotifyCollectionChanged _notifier;
        private NotifyCollectionChangedEventSubscription _subscription;

        public ObservableASTableDataSource(
            ObservableRangeCollection<T> items,
            ASTableNode tableNode,
            Func<int, IList<T>, ASCellNode> createCellFunc)
        {
            _tableNodeRef = WeakReferenceEx.Create(tableNode);
            _mainThread = Thread.CurrentThread;
            _createCellFunc = createCellFunc;

            DataSource = items;
        }

        public ObservableRangeCollection<T> DataSource
        {
            get => _dataSource;
            private set
            {
                if (Equals(_dataSource, value))
                {
                    return;
                }

                _dataSource = value;
                _notifier = value;

                if (_notifier != null)
                {
                    _subscription = new NotifyCollectionChangedEventSubscription(DataSource, HandleCollectionChanged);
                }

                _tableNodeRef.Target?.ReloadData();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _subscription?.Dispose();
            }

            base.Dispose(disposing);
        }

        public event EventHandler DataReloaded;

        public override nint NumberOfSectionsInTableNode(ASTableNode tableNode)
        {
            return 1;
        }

        public override nint NumberOfRowsInSection(ASTableNode tableNode, nint section)
        {
            return _dataSource.Count;
        }

        public override ASCellNodeBlock NodeBlockForRowAtIndexPath(ASTableNode tableNode, NSIndexPath indexPath)
        {
            return () => _createCellFunc(indexPath.Row, DataSource);
        }

        private void HandleCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (_tableNodeRef.Target?.View == null)
            {
                return;
            }

            void act()
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        {
                            var count = e.NewItems.Count;
                            var paths = new NSIndexPath[count];

                            for (var i = 0; i < count; i++)
                            {
                                paths[i] = NSIndexPath.FromRowSection(e.NewStartingIndex + i, 0);
                            }

                            _tableNodeRef.Target.View.InsertRows(paths, UITableViewRowAnimation.None);
                        }
                        break;

                    case NotifyCollectionChangedAction.Remove:
                        {
                            var count = e.OldItems.Count;
                            var paths = new NSIndexPath[count];

                            for (var i = 0; i < count; i++)
                            {
                                var index = NSIndexPath.FromRowSection(e.OldStartingIndex + i, 0);
                                paths[i] = index;
                            }
                            _tableNodeRef.Target.View.DeleteRows(paths, UITableViewRowAnimation.Automatic);
                        }
                        break;

                    default:
                        _tableNodeRef.Target.ReloadData();
                        DataReloaded?.Invoke(this, EventArgs.Empty);
                        break;
                }
            }

            var isMainThread = Thread.CurrentThread == _mainThread;
            if (isMainThread)
            {
                act();
            }
            else
            {
                NSOperationQueue.MainQueue.AddOperation(act);
                NSOperationQueue.MainQueue.WaitUntilAllOperationsAreFinished();
            }
        }
    }
}
