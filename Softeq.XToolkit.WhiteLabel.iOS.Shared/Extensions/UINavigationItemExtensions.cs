﻿using System.Windows.Input;
using UIKit;
using FFImageLoading;
using FFImageLoading.Transformations;
using Softeq.XToolkit.WhiteLabel.iOS.Extensions;
using Softeq.XToolkit.WhiteLabel.Threading;
using static Softeq.XToolkit.WhiteLabel.iOS.Helpers.AvatarImageHelpers;

namespace Softeq.XToolkit.WhiteLabel.iOS.Shared.Extensions
{
    public static class UINavigationItemExtensions
    {
        public static async void SetSettingsButtonAsync(
            this UINavigationItem navItem,
            string name,
            string url,
            ICommand command,
            AvatarStyles styles)
        {
            var buttonImage = default(UIImage);

            if (!string.IsNullOrEmpty(url))
            {
                var expr = ImageService.Instance
                    .LoadUrl(url)
                    .DownSample(styles.Size.Width, styles.Size.Height)
                    .Transform(new CircleTransformation());

                try
                {
                    buttonImage = await expr.AsUIImageAsync().ConfigureAwait(false);
                    buttonImage = buttonImage.ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);
                }
                catch
                {
                    // do nothing
                }
            }

            Execute.BeginOnUIThread(() =>
            {
                if (buttonImage == null)
                {
                    buttonImage = CreateAvatarWithTextPlaceholder(name, styles)
                        .ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);
                }

                navItem.SetCommand(buttonImage, command, false);
            });
        }
    }
}
