﻿using AsyncDisplayKitBindings;
using FFImageLoading;
using Softeq.XToolkit.WhiteLabel.Threading;
using UIKit;
using static Softeq.XToolkit.WhiteLabel.iOS.Helpers.AvatarImageHelpers;

namespace Softeq.XToolkit.WhiteLabel.iOS.Shared.Extensions
{
    public static class ASImageNodeExtensions
    {
        public static async void LoadImageWithTextPlaceholder(this ASImageNode imageView,
            string url,
            string name,
            AvatarStyles styles)
        {
            Execute.BeginOnUIThread(() =>
            {
                imageView.Image = CreateAvatarWithTextPlaceholder(name, styles);
            });

            var expr = ImageService.Instance
                .LoadUrl(url)
                .DownSampleInDip(styles.Size.Width, styles.Size.Height);

            UIImage image = null;

            try
            {
                image = await expr.AsUIImageAsync().ConfigureAwait(false);
            }
            catch
            {
                // do nothing
            }

            if (image != null)
            {
                Execute.BeginOnUIThread(() =>
                {
                    imageView.Image = image;
                });
            }
        }
    }
}
