﻿using System.Windows.Input;
using Android.Graphics.Drawables;
using FFImageLoading;
using FFImageLoading.Transformations;
using Softeq.XToolkit.WhiteLabel.Droid.Controls;
using Softeq.XToolkit.WhiteLabel.Threading;
using static Softeq.XToolkit.WhiteLabel.Droid.Controls.AvatarPlaceholderDrawable;

namespace Softeq.XToolkit.WhiteLabel.Droid.Shared.Extensions
{
    public static class NavigationBarViewExtensions
    {
        public static async void SetSettingsButtonAsync(
            this NavigationBarView navBar,
            string userName,
            string userPhotoUrl,
            ICommand command,
            AvatarStyles styles)
        {
            var drawable = default(Drawable);

            if (!string.IsNullOrEmpty(userPhotoUrl))
            {
                var expr = ImageService.Instance
                    .LoadUrl(userPhotoUrl)
                    .DownSampleInDip(styles.Size.Width, styles.Size.Height)
                    .Transform(new CircleTransformation());

                try
                {
                    drawable = await expr.AsBitmapDrawableAsync().ConfigureAwait(false);
                }
                catch
                {
                    // do nothing
                }
            }

            if (drawable == null)
            {
                drawable = new AvatarPlaceholderDrawable(userName, styles);
            }

            Execute.BeginOnUIThread(() =>
            {
                navBar.SetRightButton(drawable, command);
            });
        }
    }
}
