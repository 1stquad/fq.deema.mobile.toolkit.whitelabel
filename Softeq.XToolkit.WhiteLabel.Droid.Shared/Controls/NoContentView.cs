﻿using System;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Widget;
using Android.Graphics;
using Android.Views;

namespace Softeq.XToolkit.WhiteLabel.Droid.Shared.Controls
{
    [Register("com.softeq.xtoolkit.whitelabel.shared.droid.controls.NoContentView")]
    public class NoContentView : RelativeLayout
    {
        private TextView _label;

        public NoContentView(Context context) : base(context)
        {
            Init(context);
        }

        public NoContentView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Init(context);
        }

        public NoContentView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
            Init(context);
        }

        public NoContentView(IntPtr handle, JniHandleOwnership owner) : base(handle, owner)
        {
        }

        public string Text
        {
            get => _label.Text;
            set => _label.Text = value;
        }

        public ViewGroup.LayoutParams TextLayoutParameters
        {
            get => _label.LayoutParameters;
            set => _label.LayoutParameters = value;
        }

        public void SetTextColor(Color color)
        {
            _label.SetTextColor(color);
        }

        private void Init(Context context)
        {
            _label = new TextView(Context);

            var layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WrapContent, LayoutParams.WrapContent);
            layoutParams.AddRule(LayoutRules.CenterInParent);
            _label.LayoutParameters = layoutParams;

            AddView(_label);
        }
    }
}
