﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.WhiteLabel.Navigation
{
    public class NavigationParameterModel
    {
        public PropertyInfoModel PropertyInfo { get; set; }
        public object Value { get; set; }
    }
}