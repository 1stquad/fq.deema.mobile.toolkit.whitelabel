﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.WhiteLabel.Model
{
    public enum Platform
    {
        Unknown,
        Android,
        iOS,
        WindowsPhone,
        Windows,
        WindowsTablet,
        SurfaceHub,
        Xbox,
        IoT,
        tvOS,
        watchOS,
        macOS,
        Tizen
    }
}