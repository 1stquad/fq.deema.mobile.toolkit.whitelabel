﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.WhiteLabel.ImagePicker
{
    public enum ImagePickerOpenTypes
    {
        Camera,
        Gallery
    }
}