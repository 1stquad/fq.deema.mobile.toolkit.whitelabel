﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.WhiteLabel.Threading
{
    public static class PlatformProvider
    {
        public static IPlatformProvider Current { get; set; }
    }
}