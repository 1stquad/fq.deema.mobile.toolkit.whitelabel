﻿// Developed by Softeq Development Corporation
// http://www.softeq.com
using System;

namespace Softeq.XToolkit.WhiteLabel.Interfaces
{
    public interface IViewModelParameter<T>
    {
        T Parameter { get; set; }
    }
}