﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

using System.Collections.Generic;
using System.Windows.Input;
using Softeq.XToolkit.WhiteLabel.Mvvm;

namespace Softeq.XToolkit.WhiteLabel.Interfaces
{
    public interface IActionSheet
    {
        ICommand OpenCommand { get; }
        void SetHeader(string header);
        void SetActions(IList<CommandAction> actions);
    }

    public interface IActionSheetFactory
    {
        IActionSheet CreateEmptyActionSheet();

        IActionSheet CreateActionSheet(string header, IList<CommandAction> actions);
    }

    public class ActionSheetFactory : IActionSheetFactory
    {
        private readonly IIocContainer _container;

        public ActionSheetFactory(IIocContainer container)
        {
            _container = container;
        }

        public IActionSheet CreateEmptyActionSheet()
        {
            return _container.Resolve<IActionSheet>();
        }

        public IActionSheet CreateActionSheet(string header, IList<CommandAction> actions)
        {
            var actionSheet = CreateEmptyActionSheet();
            actionSheet.SetHeader(header);
            actionSheet.SetActions(actions);
            return actionSheet;
        }
    }
}