﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.WhiteLabel.Location
{
    public class LocationModel
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}