﻿using Softeq.XToolkit.WhiteLabel.Shared.Interfaces;

namespace Softeq.XToolkit.WhiteLabel.Shared
{
    public class TimeTrackerFactory : ITimeTrackerFactory
    {
        public ITimeTracker Create()
        {
            return new SimpleTimeTracker();
        }
    }
}
