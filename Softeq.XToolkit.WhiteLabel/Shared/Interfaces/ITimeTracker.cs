﻿using System;

namespace Softeq.XToolkit.WhiteLabel.Shared.Interfaces
{
    public interface ITimeTracker
    {
        TimeSpan CurrentTime { get; }
        TimeSpan SummaryTime { get; }

        void Start();
        void Stop();
        void Reset();
    }
}
