﻿namespace Softeq.XToolkit.WhiteLabel.Shared.Interfaces
{
    public interface ITimeTrackerFactory
    {
        ITimeTracker Create();
    }
}
