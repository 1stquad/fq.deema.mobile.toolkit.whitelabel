using System;
using System.Linq;

namespace Softeq.XToolkit.WhiteLabel.Shared
{
    public static class AvatarPlaceholderBuilder
    {
        public static (string Text, string Color) Build(string name, string[] colors)
        {
            var abbr = GetAbbreviation(name);
            var index = Math.Abs(abbr.GetHashCode()) % (colors.Length - 1);

            return (abbr, colors[index]);
        }

        private static string GetAbbreviation(string data)
        {
            var trimedData = data.Trim();

            if (trimedData.Length == 0)
            {
                return string.Empty;
            }

            if (trimedData.Contains(' '))
            {
                var splited = trimedData.Split(' ');
                return $"{splited[0].ToUpper()[0]}{splited[1].ToUpper()[0]}";
            }

            var pascalCase = trimedData;
            pascalCase = trimedData.ToUpper()[0] + pascalCase.Substring(1);
            var upperCaseOnly = string.Concat(pascalCase.Where(char.IsUpper));

            if (upperCaseOnly.Length > 1 && upperCaseOnly.Length <= 3)
            {
                return upperCaseOnly.ToUpper();
            }

            if (trimedData.Length <= 3)
            {
                return trimedData.ToUpper();
            }

            return trimedData.Substring(0, 3).ToUpper();
        }
    }
}